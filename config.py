# ---------------------------------------------------------------------------------------------------------------------
# Project:       crawler_poloniex
# Description:   configuration
# Version:       1.7.1  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

import logging as log


"""
logger config
"""
log.basicConfig(filename='log.txt',
                level=log.DEBUG,
                format=u'%(filename)s [LINE:%(lineno)d]# '
                       u'%(levelname)-8s [%(asctime)s]  %(message)s')
