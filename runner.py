# ---------------------------------------------------------------------------------------------------------------------
# Project:       crawler_poloniex
# Description:   starts collecting data from the resource
#                and writes them to the database table.
# Version:       1.7.1  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

import requests
import json
import time
from datetime import datetime
from sqlalchemy.orm import Session

from config import log
from models import Data
from manage import engine_b


# run data collection cycle
while True:
    req = requests.get('https://poloniex.com/public?command=returnTicker')
    obj = json.loads(req.text)  # convert data
    session = Session(bind=engine_b)

    price = (obj['USDT_BTC']['last'],
             obj['USDT_ETH']['last'],
             obj['BTC_ETH']['last'])

    t = datetime.now()  # get the current date-time

    try:
        signal_a = Data(ticker='USDT-BTC',
                        price=price[0],
                        timestamp=t)
        signal_b = Data(ticker='USDT-ETH',
                        price=price[1],
                        timestamp=t)
        signal_c = Data(ticker='BTC-ETH',
                        price=price[2],
                        timestamp=t)
        session.add_all([signal_a,
                         signal_b,
                         signal_c])  # insert data into a table
        session.commit()  # confirm changes
    except Exception as e:
        session.rollback()
        # terminate the session in the event of an exception
        log.error(e)
    time.sleep(60)
